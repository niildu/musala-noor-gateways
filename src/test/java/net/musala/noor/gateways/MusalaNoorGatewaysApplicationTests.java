package net.musala.noor.gateways;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.musala.noor.gateways.MusalaNoorGatewaysApplication;

@SpringBootTest(
		classes = {MusalaNoorGatewaysApplication.class},
		webEnvironment = WebEnvironment.RANDOM_PORT
)
class MusalaNoorGatewaysApplicationTests {

	@Test
	void contextLoads() {
	}

}
