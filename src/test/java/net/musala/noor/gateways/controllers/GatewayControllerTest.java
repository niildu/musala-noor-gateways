package net.musala.noor.gateways.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.RestTemplate;

import com.musala.noor.gateways.MusalaNoorGatewaysApplication;
import com.musala.noor.gateways.entity.DeviceStatus;
import com.musala.noor.gateways.model.DeviceData;
import com.musala.noor.gateways.model.DeviceRequest;
import com.musala.noor.gateways.model.GatewayData;
import com.musala.noor.gateways.model.GatewayRequest;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 08 Nov 2020
 *
 */
@SpringBootTest(
		classes = {MusalaNoorGatewaysApplication.class},
		webEnvironment = WebEnvironment.RANDOM_PORT
)
class GatewayControllerTest {

	private String baseUrl;
	private RestTemplate restTemplate = new RestTemplate();;
	@LocalServerPort int randomServerPort;

	@BeforeEach
	void setUp() throws Exception {
		baseUrl = String.format("http://localhost:%d/gateway", randomServerPort);
	}

	@AfterEach
	void tearDown() throws Exception {
		ResponseEntity<GatewayData[]> result = restTemplate.getForEntity(baseUrl, GatewayData[].class);
		for (GatewayData gw : result.getBody()) {
			restTemplate.delete(baseUrl + "/{serialNumber}", gw.getSerialNumber());
		}
	}

	@Test
	void test01AddGateway() {
		Assertions.assertThrows(BadRequest.class, () -> {
			addGateway("1111111111", "Test 1", "192.168.0.xxx"); // Invalid ipv4 address
		});
		Assertions.assertThrows(BadRequest.class, () -> {
			addGateway(null, "Test 1", "192.168.0.123"); // Mandatory serial number not provided
		});
		Assertions.assertThrows(BadRequest.class, () -> {
			addGateway("1111111111", null, "192.168.0.123"); // Mandatory name not provided
		});
		ResponseEntity<GatewayData> result = addGateway("1111111111", "Test 1", "192.168.0.123");
		assertEquals(200, result.getStatusCodeValue());
		assertNotNull(result.getBody());

		Assertions.assertThrows(BadRequest.class, () -> {
			addGateway("1111111111", "Test 1", "192.168.0.123"); // duplicate serial
		});
	}

	@Test
	void testGetAllGateways() {
		ResponseEntity<GatewayData[]> result = restTemplate.getForEntity(baseUrl, GatewayData[].class);
		assertEquals(200, result.getStatusCodeValue());
		assertEquals(0, result.getBody().length); // Records size is 0

		addGateway("1111111111", "Test 1", "192.168.0.123");
		result = restTemplate.getForEntity(baseUrl, GatewayData[].class);
		assertEquals(200, result.getStatusCodeValue());
		assertEquals(1, result.getBody().length); // Records size is 1

		addGateway("1111111112", "Test 2", "192.168.0.124");
		result = restTemplate.getForEntity(baseUrl, GatewayData[].class);
		assertEquals(200, result.getStatusCodeValue());
		assertEquals(2, result.getBody().length); // Records size is 2
	}

	@Test
	void testGetGateway() {
		Assertions.assertThrows(BadRequest.class, () -> {
			restTemplate.getForEntity(baseUrl + "/notavalidserialnumber", GatewayData.class);
		});
		String sn = "999999999";
		addGateway(sn, "Test 1", "192.168.0.123");
		ResponseEntity<GatewayData> result = restTemplate.getForEntity(baseUrl + "/" + sn, GatewayData.class);
		assertEquals(200, result.getStatusCodeValue());
		assertEquals(sn, result.getBody().getSerialNumber());
	}

	@Test
	void testDeleteGateway() {
		Assertions.assertThrows(BadRequest.class, () -> {
			restTemplate.delete(baseUrl + "/{serialNumber}", "notavalidserialnumber");
		});
		String sn = "88888888";
		addGateway(sn, "Test 1", "192.168.0.123");
		restTemplate.delete(baseUrl + "/{serialNumber}", sn);
	}

	@Test
	void testAddDevice() {
		String sn = "77777777";
		Long uid = 9999l;
		String vendor = "Vendor 1";
		DeviceStatus stat = DeviceStatus.ONLINE;

		// Invalid serial
		Assertions.assertThrows(BadRequest.class, () -> {
			addDevice(sn, uid, vendor, stat);
		});
		// Add a valid gateway
		addGateway(sn, "Test 1", "192.168.0.123");
		// Invalid UID
		Assertions.assertThrows(BadRequest.class, () -> {
			addDevice(sn, null, vendor, stat);
		});
		// Invalid vendor
		Assertions.assertThrows(BadRequest.class, () -> {
			addDevice(sn, uid, null, stat);
		});
		// Invalid status
		Assertions.assertThrows(BadRequest.class, () -> {
			addDevice(sn, uid, vendor, null);
		});
		// Add a valid device
		ResponseEntity<DeviceData> result = addDevice(sn, uid, vendor, stat);
		assertEquals(200, result.getStatusCodeValue());
		assertNotNull(result.getBody());
		assertEquals(uid, result.getBody().getUid());
		// Duplicate device entry
		Assertions.assertThrows(BadRequest.class, () -> {
			addDevice(sn, uid, vendor, stat);
		});
		// add 9 more devices to the gateway
		for (int i = 1; i < 10; i++) {
			addDevice(sn, uid + i, vendor + i, stat);
		}
		// Gateway full error
		Assertions.assertThrows(BadRequest.class, () -> {
			addDevice(sn, uid + 99, vendor + " => 22", stat);
		});
	}

	@Test
	void testGetAllDevices() {
		String url = baseUrl + "/{serialNumber}/device";
		String sn = "66666666";
		Long uid = 8888l;
		String vendor = "Vendor 1";
		DeviceStatus stat = DeviceStatus.ONLINE;

		// Invalid serial
		Assertions.assertThrows(BadRequest.class, () -> {
			restTemplate.getForEntity(url, DeviceData[].class, sn);
		});
		// Add a valid gateway
		addGateway(sn, "Test 1", "192.168.0.123");
		// Device list is empty
		ResponseEntity<DeviceData[]> result = restTemplate.getForEntity(url, DeviceData[].class, sn);
		assertEquals(200, result.getStatusCodeValue());
		assertEquals(0, result.getBody().length); // Records size is 0
		// Add a device
		addDevice(sn, uid, vendor, stat);
		// Device list size 1
		result = restTemplate.getForEntity(url, DeviceData[].class, sn);
		assertEquals(200, result.getStatusCodeValue());
		assertEquals(1, result.getBody().length); // Records size is 1
		// Add another device
		addDevice(sn, uid + 4, vendor, stat);
		// Device list size 2
		result = restTemplate.getForEntity(url, DeviceData[].class, sn);
		assertEquals(200, result.getStatusCodeValue());
		assertEquals(2, result.getBody().length); // Records size is 2
	}

	@Test
	void testGetDevice() {
		String url = baseUrl + "/{serialNumber}/device/{uid}";
		String sn = "444444444";
		Long uid = 77777l;
		String vendor = "Vendor 1";
		DeviceStatus stat = DeviceStatus.ONLINE;
		// Add a valid gateway
		addGateway(sn, "Test 1", "192.168.0.123");
		// Invalid UID
		Assertions.assertThrows(BadRequest.class, () -> {
			restTemplate.getForEntity(url, DeviceData.class, sn, uid);
		});
		// Add a device
		addDevice(sn, uid, vendor, stat);
		// Valid
		ResponseEntity<DeviceData> result = restTemplate.getForEntity(url, DeviceData.class, sn, uid);
		assertEquals(200, result.getStatusCodeValue());
		assertNotNull(result.getBody());
		assertEquals(uid, result.getBody().getUid());
	}

	@Test
	void testDeleteDevice() {
		String url = baseUrl + "/{serialNumber}/device/{uid}";
		String sn = "3333333";
		Long uid = 66666l;
		String vendor = "Vendor 1";
		DeviceStatus stat = DeviceStatus.ONLINE;
		// Invalid serial number and UID
		Assertions.assertThrows(BadRequest.class, () -> {
			restTemplate.delete(url, sn, uid);
		});
		// Add a valid gateway
		addGateway(sn, "Test 1", "192.168.0.123");
		// Invalid UID
		Assertions.assertThrows(BadRequest.class, () -> {
			restTemplate.delete(url, sn, uid);
		});
		// Add a device
		addDevice(sn, uid, vendor, stat);
		// Valid delete request
		restTemplate.delete(url, sn, uid);
	}

	private ResponseEntity<DeviceData> addDevice(String serialNumber, Long uid, String vendor, DeviceStatus status) {
		return restTemplate.postForEntity(baseUrl + "/{serialNumber}/device", DeviceRequest.builder()
				.uid(uid)
				.vendor(vendor)
				.status(status)
				.build(), DeviceData.class, serialNumber);
	}

	private ResponseEntity<GatewayData> addGateway(String serialNumber, String name, String ipv4Address) {
		return restTemplate.postForEntity(baseUrl, GatewayRequest.builder()
				.serialNumber(serialNumber)
				.name(name)
				.ipv4Address(ipv4Address)
				.build(), GatewayData.class);
	}

}
