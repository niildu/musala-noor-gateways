package com.musala.noor.gateways.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.musala.noor.gateways.entity.Device;
import com.musala.noor.gateways.entity.Gateway;
import com.musala.noor.gateways.exception.InvalidRequestException;
import com.musala.noor.gateways.model.DeviceData;
import com.musala.noor.gateways.model.DeviceRequest;
import com.musala.noor.gateways.model.GatewayData;
import com.musala.noor.gateways.model.GatewayRequest;
import com.musala.noor.gateways.repo.DeviceRepo;
import com.musala.noor.gateways.repo.GatewayRepo;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@RestController
@RequestMapping("/gateway")
@ApiOperation(tags = "Gateway API", value = "Musala Gateway API")
public class GatewayController {

	@Value("${gateway.devicecount.max:10}")
	private Integer maxDeviceCount;

	@Autowired GatewayRepo gatewayRepo;
	@Autowired DeviceRepo deviceRepo;

	@PostMapping(produces = APPLICATION_JSON_VALUE)
	@ApiOperation(tags="Gateway", value="Add gateway", notes="Add a new gateway")
	public GatewayData addGateway(
			@ApiParam(value="Gateway request to add", required=true) @Valid @RequestBody GatewayRequest req) {
		gatewayRepo.findBySerialNumber(req.getSerialNumber()).ifPresent(g -> {
			throw new InvalidRequestException(String.format("Gateway with serial number %s already exists", req.getSerialNumber()));
		});

		return convGatewayToData(gatewayRepo.save(Gateway.builder()
				.serialNumber(req.getSerialNumber())
				.name(req.getName())
				.ipv4Address(req.getIpv4Address())
				.build()));
	}

	@GetMapping(produces = APPLICATION_JSON_VALUE)
	@ApiOperation(tags="Gateway", value="Get gateways", notes="Get list of all gateways")
	public List<GatewayData> getAllGateways() {
		return gatewayRepo.findAll().stream().map(this::convGatewayToData).collect(Collectors.toList());
	}

	@GetMapping(value = "/{serialNumber}", produces = APPLICATION_JSON_VALUE)
	@ApiOperation(tags="Gateway", value="Get gateway", notes="Get a gateway details")
	public GatewayData getGateway(
			@ApiParam(value="Gateway serial number", required=true) @PathVariable String serialNumber) {
		return convGatewayToData(getGatewayFromSerial(serialNumber));
	}

	@DeleteMapping(value = "/{serialNumber}", produces = APPLICATION_JSON_VALUE)
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	@ApiOperation(tags="Gateway", value="Delete gateway", notes="Delete a gateway")
	public void deleteGateway(
			@ApiParam(value="Gateway serial number", required=true) @PathVariable String serialNumber) {
		gatewayRepo.delete(getGatewayFromSerial(serialNumber));
	}

	@PostMapping(value = "/{serialNumber}/device", produces = APPLICATION_JSON_VALUE)
	@ApiOperation(tags="Device", value="Add device", notes="Add a device to the gateway")
	public DeviceData addDevice(
			@ApiParam(value="Gateway serial number", required=true) @PathVariable String serialNumber,
			@ApiParam(value="Device request to add", required=true) @Valid @RequestBody DeviceRequest req) {
		Gateway gw = getGatewayFromSerial(serialNumber);
		if (gw.getDevices().size() >= maxDeviceCount) {
			throw new InvalidRequestException("The gateway is full");
		}

		deviceRepo.findByGatewayAndUid(gw, req.getUid()).ifPresent(d -> {
			throw new InvalidRequestException(String.format("Device with UID %d already exists for gateway %s (%s)", req.getUid(), gw.getName(), gw.getSerialNumber()));
		});

		return convDeviceToData(deviceRepo.save(Device.builder()
				.gateway(gw)
				.uid(req.getUid())
				.vendor(req.getVendor())
				.status(req.getStatus())
				.build()));
	}

	@GetMapping(value = "/{serialNumber}/device", produces = APPLICATION_JSON_VALUE)
	@ApiOperation(tags="Device", value="Get devices", notes="Get list of all devices")
	public List<DeviceData> getAllDevices(
			@ApiParam(value="Gateway serial number", required=true) @PathVariable String serialNumber) {
		Gateway gw = getGatewayFromSerial(serialNumber);
		return deviceRepo.findAllByGateway(gw).stream().map(this::convDeviceToData).collect(Collectors.toList());
	}

	@GetMapping(value = "/{serialNumber}/device/{uid}", produces = APPLICATION_JSON_VALUE)
	@ApiOperation(tags="Device", value="Get device", notes="Get a device details")
	public DeviceData getDevice(
			@ApiParam(value="Gateway serial number", required=true) @PathVariable String serialNumber,
			@ApiParam(value="Device UID", required=true) @PathVariable Long uid) {
		return convDeviceToData(getDeviceFromGatewayAndUID(getGatewayFromSerial(serialNumber), uid));
	}

	@DeleteMapping(value = "/{serialNumber}/device/{uid}", produces = APPLICATION_JSON_VALUE)
	@ResponseStatus(value=HttpStatus.NO_CONTENT)
	@ApiOperation(tags="Device", value="Delete gateway", notes="Delete a gateway")
	public void deleteDevice(
			@ApiParam(value="Gateway serial number", required=true) @PathVariable String serialNumber,
			@ApiParam(value="Device UID", required=true) @PathVariable Long uid) {
		deviceRepo.delete(getDeviceFromGatewayAndUID(getGatewayFromSerial(serialNumber), uid));
	}

	private Gateway getGatewayFromSerial(String serialNumber) {
		Optional<Gateway> gwop = gatewayRepo.findBySerialNumber(serialNumber);
		if (!gwop.isPresent()) {
			throw new InvalidRequestException(String.format("Gateway not found with serial number %s", serialNumber));
		}
		return gwop.get();
	}
	
	private Device getDeviceFromGatewayAndUID(Gateway gateway, Long uid) {
		Optional<Device> devop = deviceRepo.findByGatewayAndUid(gateway, uid);
		if (!devop.isPresent()) {
			throw new InvalidRequestException(String.format("Device with UID %d for gateway serial number %s not found", uid, gateway.getSerialNumber()));
		}
		return devop.get();
	}
	
	private GatewayData convGatewayToData(Gateway gw) {
		return GatewayData.builder()
				.serialNumber(gw.getSerialNumber())
				.name(gw.getName())
				.ipv4Address(gw.getIpv4Address())
				.devices(gw.getDevices() == null ? null : gw.getDevices().stream().map(this::convDeviceToData).collect(Collectors.toList()))
				.build();
	}

	private DeviceData convDeviceToData(Device d) {
		return DeviceData.builder()
				.uid(d.getUid())
				.vendor(d.getVendor())
				.created(d.getCreated())
				.status(d.getStatus())
				.build();
	}
}
