package com.musala.noor.gateways.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.musala.noor.gateways.entity.DeviceStatus;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description="Model to bind device details")
public class DeviceRequest {
	@NotNull
	@ApiModelProperty(value = "UID", dataType = "long", required = true, example = "99")
	private Long uid;
	@NotEmpty(message = "Vendor can't be empty")
	@ApiModelProperty(value = "Vendor", dataType = "string", required = true, example = "xx")
	private String vendor;
	@NotNull
	@ApiModelProperty(value = "ONLINE/OFFLINE", dataType = "string", required = true, example = "ONLINE/OFFLINE")
	private DeviceStatus status;
}
