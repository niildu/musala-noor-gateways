package com.musala.noor.gateways.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description="Model to bind gateway details")
public class GatewayRequest {
	@NotEmpty(message = "Serial number can't be empty")
	@ApiModelProperty(value = "Serial number", dataType = "String", required = true, example = "xx")
	private String serialNumber;
	@NotEmpty(message = "Name can't be empty")
	@ApiModelProperty(value = "Name", dataType = "String", required = true, example = "xx")
	private String name;
	@Pattern(regexp = "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.(?!$)|$)){4}$", message="Invalid IPv4 address")
	@ApiModelProperty(value = "IPv4 address", dataType = "String", required = true, example = "192.168.0.123")
	private String ipv4Address;
}
