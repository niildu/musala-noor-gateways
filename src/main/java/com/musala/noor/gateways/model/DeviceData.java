package com.musala.noor.gateways.model;

import java.time.LocalDateTime;

import com.musala.noor.gateways.entity.DeviceStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceData {
	private Long uid;
	private String vendor;
	private LocalDateTime created;
	private DeviceStatus status;
}
