package com.musala.noor.gateways.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GatewayData {
	private String serialNumber;
	private String name;
	private String ipv4Address;
	private List<DeviceData> devices;
}
