package com.musala.noor.gateways.exception;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@Data
@AllArgsConstructor
public class ExceptionResponse {
	private LocalDateTime timestamp;
	private String message;
	private String details;
}
