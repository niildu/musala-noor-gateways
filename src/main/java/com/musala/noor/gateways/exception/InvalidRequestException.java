package com.musala.noor.gateways.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 08 Nov 2020
 *
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidRequestException extends RuntimeException {
	private static final long serialVersionUID = 3351821740745375263L;

	public InvalidRequestException(String exception) {
		super(exception);
	}
}
