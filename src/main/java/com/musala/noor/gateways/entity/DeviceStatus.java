package com.musala.noor.gateways.entity;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
public enum DeviceStatus {
	ONLINE, OFFLINE;
}
