package com.musala.noor.gateways.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Gateway {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "gateway_id_generator")
	private Long id;
	@Column(nullable = false, unique = true)
	private String serialNumber;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private String ipv4Address;
	@OneToMany(targetEntity = Device.class, mappedBy = "gateway", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<Device> devices;
}
