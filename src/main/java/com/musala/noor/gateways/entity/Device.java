package com.musala.noor.gateways.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Device {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "device_id_generator")
	private Long id;
	@Column(nullable = false)
	private Long uid;
	@Column(nullable = false)
	private String vendor;
	@Column(nullable = false)
	private LocalDateTime created;
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private DeviceStatus status;
	@ManyToOne(optional = false, targetEntity = Gateway.class)
	private Gateway gateway;

	@PrePersist
	void onCreate() {
		this.created = LocalDateTime.now();
	}
}
