package com.musala.noor.gateways.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.noor.gateways.entity.Device;
import com.musala.noor.gateways.entity.Gateway;

/**
 * @author Noor Siddique
 * @email niildu@gmail.com
 * @since 07 Nov 2020
 *
 */
@Repository
public interface DeviceRepo extends JpaRepository<Device, Long> {

	Optional<Device> findByGatewayAndUid(Gateway gateway, Long uid);
	List<Device> findAllByGateway(Gateway gateway);
}
