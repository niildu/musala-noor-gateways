# Musala Practical Test - Gateway API

## Technologies
* Java 8
* Spring Boot 2.3.5.RELEASE
* In memory database H2
* Rest client Swagger UI 3.0.0
* Build tool Maven

## Guides
* Repository URL [https://bitbucket.org/niildu/musala-noor-gateways](https://bitbucket.org/niildu/musala-noor-gateways)
* Clone `git clone https://niildu@bitbucket.org/niildu/musala-noor-gateways.git`
* Build jar `mvn clean install`
* Execute jar `java -jar musala-noor-gateways-0.0.1-SNAPSHOT.jar`
* Run Spring Boot application from source `mvn spring-boot:run`
* Rest Client Swagger UI URL: `http://localhost:8080/swagger-ui/index.html`

## Heroku
The application is deployed in heroku server. It doesn't persist any data and is stopped by default. It starts on first attempt which takes sometime to start the application. Everytime the app starts from scratch!

URL: [https://musala-noor-gateways.herokuapp.com/swagger-ui/index.html](https://musala-noor-gateways.herokuapp.com/swagger-ui/index.html)

## API Details
```json
{
  "swagger": "2.0",
  "info": {
    "description": "For Musala",
    "version": "0.0.1",
    "title": "Musala Practical Test - Gateway API",
    "contact": {
      "name": "Md Noor Mohammad Siddique",
      "email": "niildu@gmail.com"
    }
  },
  "host": "localhost:8080",
  "basePath": "/",
  "tags": [
    {
      "name": "gateway-controller",
      "description": "Gateway Controller"
    }
  ],
  "paths": {
    "/gateway": {
      "get": {
        "tags": [
          "Gateway"
        ],
        "summary": "Get gateways",
        "description": "Get list of all gateways",
        "operationId": "getAllGatewaysUsingGET",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/GatewayData"
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      },
      "post": {
        "tags": [
          "Gateway"
        ],
        "summary": "Add gateway",
        "description": "Add a new gateway",
        "operationId": "addGatewayUsingPOST",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "req",
            "description": "Gateway request to add",
            "required": true,
            "schema": {
              "$ref": "#/definitions/GatewayRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/GatewayData"
            }
          },
          "201": {
            "description": "Created"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    },
    "/gateway/{serialNumber}": {
      "get": {
        "tags": [
          "Gateway"
        ],
        "summary": "Get gateway",
        "description": "Get a gateway details",
        "operationId": "getGatewayUsingGET",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "serialNumber",
            "in": "path",
            "description": "Gateway serial number",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/GatewayData"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      },
      "delete": {
        "tags": [
          "Gateway"
        ],
        "summary": "Delete gateway",
        "description": "Delete a gateway",
        "operationId": "deleteGatewayUsingDELETE",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "serialNumber",
            "in": "path",
            "description": "Gateway serial number",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "204": {
            "description": "No Content"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          }
        }
      }
    },
    "/gateway/{serialNumber}/device": {
      "get": {
        "tags": [
          "Device"
        ],
        "summary": "Get devices",
        "description": "Get list of all devices",
        "operationId": "getAllDevicesUsingGET",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "serialNumber",
            "in": "path",
            "description": "Gateway serial number",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/DeviceData"
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      },
      "post": {
        "tags": [
          "Device"
        ],
        "summary": "Add device",
        "description": "Add a device to the gateway",
        "operationId": "addDeviceUsingPOST",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "req",
            "description": "Device request to add",
            "required": true,
            "schema": {
              "$ref": "#/definitions/DeviceRequest"
            }
          },
          {
            "name": "serialNumber",
            "in": "path",
            "description": "Gateway serial number",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DeviceData"
            }
          },
          "201": {
            "description": "Created"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      }
    },
    "/gateway/{serialNumber}/device/{uid}": {
      "get": {
        "tags": [
          "Device"
        ],
        "summary": "Get device",
        "description": "Get a device details",
        "operationId": "getDeviceUsingGET",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "serialNumber",
            "in": "path",
            "description": "Gateway serial number",
            "required": true,
            "type": "string"
          },
          {
            "name": "uid",
            "in": "path",
            "description": "Device UID",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/DeviceData"
            }
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          },
          "404": {
            "description": "Not Found"
          }
        }
      },
      "delete": {
        "tags": [
          "Device"
        ],
        "summary": "Delete gateway",
        "description": "Delete a gateway",
        "operationId": "deleteDeviceUsingDELETE",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "serialNumber",
            "in": "path",
            "description": "Gateway serial number",
            "required": true,
            "type": "string"
          },
          {
            "name": "uid",
            "in": "path",
            "description": "Device UID",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "204": {
            "description": "No Content"
          },
          "401": {
            "description": "Unauthorized"
          },
          "403": {
            "description": "Forbidden"
          }
        }
      }
    }
  },
  "definitions": {
    "DeviceData": {
      "type": "object",
      "properties": {
        "created": {
          "type": "string",
          "format": "date-time"
        },
        "status": {
          "type": "string",
          "enum": [
            "OFFLINE",
            "ONLINE"
          ]
        },
        "uid": {
          "type": "integer",
          "format": "int64"
        },
        "vendor": {
          "type": "string"
        }
      },
      "title": "DeviceData"
    },
    "DeviceRequest": {
      "type": "object",
      "required": [
        "status",
        "uid",
        "vendor"
      ],
      "properties": {
        "status": {
          "type": "object",
          "example": "ONLINE/OFFLINE",
          "description": "ONLINE/OFFLINE"
        },
        "uid": {
          "type": "object",
          "example": 99,
          "description": "UID"
        },
        "vendor": {
          "type": "object",
          "example": "xx",
          "description": "Vendor"
        }
      },
      "title": "DeviceRequest",
      "description": "Model to bind device details"
    },
    "GatewayData": {
      "type": "object",
      "properties": {
        "devices": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/DeviceData"
          }
        },
        "ipv4Address": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "serialNumber": {
          "type": "string"
        }
      },
      "title": "GatewayData"
    },
    "GatewayRequest": {
      "type": "object",
      "required": [
        "ipv4Address",
        "name",
        "serialNumber"
      ],
      "properties": {
        "ipv4Address": {
          "type": "object",
          "example": "192.168.0.123",
          "description": "IPv4 address"
        },
        "name": {
          "type": "object",
          "example": "xx",
          "description": "Name"
        },
        "serialNumber": {
          "type": "object",
          "example": "xx",
          "description": "Serial number"
        }
      },
      "title": "GatewayRequest",
      "description": "Model to bind gateway details"
    }
  }
}
```